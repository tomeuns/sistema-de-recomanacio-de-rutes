## Application

### Execute
`pyton3.6 application/Main.py`

### Files generate

### Classes and Functions

#### RouteCollection
Class that contains the information of all the routes analyzed

    - from_codes: Generate by name of files of routes
    - generate distincts distributions 

#### RouteCollectionDistributions
Class that create random values based on a distribution. The distributions is created by route collection.

    - generate_distance_of_route_by_probability
    - generate_distance_between_points_by_probability
    - generate_time_between_points_by_probability
    - generate_init_point_by_probability

#### Distribution

The logical of this class is generate a distribution from a list of values.

    - __init__: Generate de distribution.
    - Generate images of distribution.

## Develop tasks

#### Read data
- [x] Read data of gpx files.
- [x] Read OSM zone.
- [x] Generate data structure(KDTree) of OSM points.
- [x] Find real point nearest to gps point.
- [ ] Read shape by zone name
- [ ] Add position in route and identifier of route in KDTree
#### Generate distributions and value by probability
- [x] Init point of route.
- [x] Distance of route.
- [x] Time of as route
- [x] Distance between points.  
- [x] Time between points.
- [x] Distance between gps point and near real point
- [ ] Angle de referència entre dos punts consecutius
- [ ] Error(Need find real point nearest).
- [ ] Diferència entre distancia real de la ruta i distància gps
#### Generate route
- [x] Use distance of route
- [ ] Generate point by distance and angle

## Vocabulary

- **Point**: Informació d'un punt en el mapa(latitud, longitud)
- **Route**: Informació d'una ruta de gps.
- **RouteCollection**: Es el resum(mitjanes, punts...) d'un conjunt de rutes.
- **Shape**: Informació consultada de l'OSM, són punts reals.

## Others
#### Shape vs ShapeNew
La diferència es per a llegir informació de OSM.

El shape empra l'api:

`api.query('way(' + way_limits + ')["highway"];(._;>;);out body;')`
~~~~
El shapeNew empra:

`osmnx.graph_from_place(place_name)`

## Problems
Que ocurre con las propiedades 'relacionadas', por ejemplo, distancia entre puntos y tiempo entre puntos