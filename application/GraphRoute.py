import matplotlib.pyplot as plt
import pandas as pd
import numpy as np


class GraphRoute:

    def __init__(self, route):
        self.route = route

    def print_route(self):
        for point in self.route.points:
            plt.scatter(point.longitude, point.latitude, s=5, c="blue")
        plt.show()

    def paint_distance_time(self):
        list_of_points = []
        accumulate_x = 0
        actual_point = self.route.points[0]
        accumulate_y = 0

        for point in self.route.points:
            accumulate_x = actual_point.meters_to_point(point) + accumulate_x
            accumulate_y = actual_point.seconds_to_point(point) + accumulate_y
            actual_point = point
            list_of_points.append([accumulate_x, accumulate_y])
        list_of_points = np.array(list_of_points)
        for point in list_of_points:
            plt.plot(list_of_points[:, 0], list_of_points[:, 1] / 3600, c="red")
        plt.xlabel('Distance in meters')
        plt.ylabel('Time in hours')
        plt.show()

    def paint_uneveness(self):
        list_of_points = []
        actual_point = self.route.points[0]
        variable_x = 0

        for point in self.route.points:
            variable_x = actual_point.meters_to_point(point) + variable_x
            variable_y = point.elevation
            actual_point = point
            list_of_points.append([variable_x, variable_y])
        list_of_points = np.array(list_of_points)
        for point in list_of_points:
            plt.plot(list_of_points[:, 0], list_of_points[:, 1], c="red")
        plt.xlabel('Distance in meters')
        plt.ylabel('Elevation')
        plt.show()
