import datetime
import os

from application.GenerateRoutesService import GenerateRoutesService
from application.Point import Point
from application.RouteCollection import RouteCollection
from application.RouteCollectionDistributions import RouteCollectionDistributions


def main():
    print(str(datetime.datetime.now()) + ': Start')
    print(str(datetime.datetime.now()) + ': Reading routes')
    real_routes = RouteCollection.read_routes(zone='santamargalida')
    print(str(datetime.datetime.now()) + ': Generating shape information')
    print(str(datetime.datetime.now()) + ': Creating route collections of reals distributions')
    route_collection_distribution = RouteCollectionDistributions('Real routes', real_routes)
    print(str(datetime.datetime.now()) + ': Created reals distributions')

    print(str(datetime.datetime.now()) + ': Generating routes')
    generated_routes = GenerateRoutesService(route_collection_distribution, 10).generate()
    print(str(datetime.datetime.now()) + ': Generating routes with OSM')
    generated_routes_with_osm = GenerateRoutesService(route_collection_distribution, 2, active_reals_points=True).generate()
    generated_routes.save(2, suffix_name='-with-osm')
    print(str(datetime.datetime.now()) + ': Creating route collections of generated distributions')
    route_collection_distribution_generated = RouteCollectionDistributions('Generated routes', generated_routes)
    route_collection_distribution_generated_with_osm = RouteCollectionDistributions('Generated routes with OSM info.', generated_routes_with_osm)
    print(str(datetime.datetime.now()) + ': Creating graphs of generated distributions')
    RouteCollectionDistributions.create_graphs_of_multiple_distributions(route_collection_distribution, route_collection_distribution_generated,
                                                                         route_collection_distribution_generated_with_osm)
    print(str(datetime.datetime.now()) + ': End')


# def generate_init_points_by_probability(quantity, route_collection_distribution):
#     init_points_generated = []
#     for i in range(quantity):
#         init_point = route_collection_distribution.generate_init_point_by_probability()
#         init_points_generated.append(init_point)
#     return init_points_generated


# def generate_distribution_of_distances_of_routes(quantity, rcd):
#     generated_distances = []
#     for _ in range(quantity):
#         generated_distances.append(rcd.generate_distance_of_route_by_probability())
#     return Distribution(generated_distances, 'Distribution of distance of route' + str(quantity) + ' points', 'meters')


# def generate_distances_points_by_probability(quantity, rcd):
#     generate_distances_points = []
#     for _ in range(quantity):
#         generate_distances_points.append(rcd.generate_distance_between_points_by_probability())
#     return Distribution(generate_distances_points, 'Distribution of distance between points of gps in meters with ' + str(quantity) + ' points',
#                         'meters')


# def generate_time_between_points_by_probability(quantity, rcd):
#     generate_time_between_points = []
#     for _ in range(quantity):
#         generate_time_between_points.append(rcd.generate_time_between_points_by_probability())
#     return Distribution(generate_time_between_points, 'Time between points of gps in ¿hours? with ' + str(quantity) + ' points')


def show_information(title, points, tree):
    print(title)
    # dist, indices = tree.query(point_list)
    match = 0
    notmatch = 0
    for point in points:
        nearest_distance_point, nearest_value_position = tree.query(point)
        nearest_point = Point(tree.data.item(nearest_value_position, 1), tree.data.item(nearest_value_position, 0))
        current_point = Point(point[0], point[1])
        distance = nearest_point.meters_to_point(current_point)
        print('Distance (m):' + str(distance) + "-----" + str(nearest_distance_point))
        if (nearest_distance_point * 1000) < 5:
            match += 1
        else:
            notmatch += 1
    print('---------SUMMARY------')
    print('Points in zone vs Points out zone')
    print(str(match) + '--' + str(notmatch))
    print('##############################')


# def paint_distances_graphs(route_collection):
#     paint_distribution("Distances of route distribution", route_collection, "Distance (m)")
#
#
# def paint_points_distances_graphs(route_collection):
#     paint_distribution("Points distances distribution", route_collection, "Distance (m)")
#
#
# def paint_points_time_graphs(route_collection):
#     paint_distribution("Points time distribution", route_collection, "Time (s)")
#
#
# def paint_distribution(title, distribution, xtitle):
#     ax = distribution.plot(drawstyle='steps', label="Hiking", legend="True")
#     ax.set_ylabel("Cumulative sum")
#     ax.set_xlabel(xtitle)
#     ax.grid(True)
#     plt.show()


if __name__ == "__main__":
    main()
