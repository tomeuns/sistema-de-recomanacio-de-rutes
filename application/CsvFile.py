import csv


class CsvFile:

    def __init__(self, points, file_name='points_file'):
        self.fileName = 'file_' + file_name + '.csv'
        self.__save_points(points)

    @classmethod
    def from_route(cls, route, file_name: str):
        return cls(route.points, file_name)

    def __save_points(self, points):
        with open(self.fileName, mode='w') as points_file:
            self.__writer = csv.writer(points_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            self.__writer.writerow(['longitude', 'latitude'])
            for point in points:
                self.__save_point_in_file(point)

    def __save_point_in_file(self, point):
        self.__writer.writerow([point.longitude, point.latitude])
