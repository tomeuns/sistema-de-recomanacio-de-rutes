import numpy
from scipy.interpolate import interp1d
from statsmodels.distributions import ECDF

from application.DistributionGraph import DistributionGraph
from application.DistributionGraphData import DistributionGraphData


class Distribution:

    def __init__(self, values, title, units, max_value=None, min_value=None):
        self.title = title
        self.units = units
        self.max_value = max_value
        self.min_value = min_value
        # if max_value is not None:
        #     values = [value for value in values if value < max_value]
        x = numpy.sort(numpy.array(values))
        self.ecdf = ECDF(x)
        self.inv_cdf = interp1d(self.ecdf.y, self.ecdf.x, bounds_error=False, assume_sorted=True)

    def value_of_probability(self) -> float:
        r = numpy.random.uniform(self.ecdf.y[1], 1)
        return self.inv_cdf(r).flat[0]

    def plot_image(self, distribution_type=None):
        distribution_graph_data = DistributionGraphData(self.ecdf.x, self.ecdf.y, '')
        DistributionGraph(self.title, self.units, distribution_graph_data, distribution_type=distribution_type, show_legend=False,
                          max_value=self.max_value, min_value=self.min_value)

    def get_data(self):
        return self.ecdf.x, self.ecdf.y
