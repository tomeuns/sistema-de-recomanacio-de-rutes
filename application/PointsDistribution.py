import random

import matplotlib.pyplot as plt
import numpy as np
import pylab

from application.Area import Area
from application.Point import Point


class PointsDistribution:
    __distanceOfGrid = 0.00001 * 250  # distancia en metros

    def __init__(self, points, area):
        self.__generate_probability_matrix(points, area)

    def __grid_position_from_coordinate(self, point, minimum):
        return int((point - minimum) / self.__distanceOfGrid)

    def __coordinate_from_grid_position(self, position, minimum):
        return position * self.__distanceOfGrid + minimum

    def __generate_probability_matrix(self, points: [Point], area: Area):
        self.__minX = area.from_point.longitude
        max_x = area.to_point.longitude
        self.__minY = area.from_point.latitude
        max_y = area.to_point.latitude

        self.__elementsX = len(np.arange(self.__minX, max_x, self.__distanceOfGrid))
        self.__elementsY = len(np.arange(self.__minY, max_y, self.__distanceOfGrid))

        self.matrix_of_occurrences = np.zeros((self.__elementsX, self.__elementsY))

        for point in points:
            x = self.__grid_position_from_coordinate(point.longitude, self.__minX)
            y = self.__grid_position_from_coordinate(point.latitude, self.__minY)
            self.matrix_of_occurrences[x, y] += 1
        total_points = np.sum(self.matrix_of_occurrences)
        probability = np.vectorize(lambda x: x / total_points)
        self.probability_matrix = probability(self.matrix_of_occurrences)

    def generate_by_probability(self):
        positions = np.arange(self.__elementsX * self.__elementsY)
        cell = np.random.choice(positions, p=self.probability_matrix.flatten())
        y = cell % self.__elementsY
        x = int((cell - y) / self.__elementsY)
        longitude = self.__coordinate_from_grid_position(x, self.__minX)
        latitude = self.__coordinate_from_grid_position(y, self.__minY)
        generate_longitude = random.uniform(longitude, longitude + self.__distanceOfGrid)
        generate_latitude = random.uniform(latitude, latitude + self.__distanceOfGrid)
        return Point(generate_latitude, generate_longitude)

    def plot_image(self, distribution_type: str):
        pylab.plot()
        plt.imshow(self.matrix_of_occurrences)
        plt.colorbar()
        plt.title("Points by zones")
        plt.xlabel("meters")
        plt.savefig("mypointsgrapsh" + distribution_type)
