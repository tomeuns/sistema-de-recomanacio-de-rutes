import matplotlib.pyplot as plt
import numpy as np

#DELETE CLASS
class GraphRouteCollection:

    def __init__(self, route_collection):
        self.route_collection = route_collection

    def points_per_route(self):
        fig = plt.figure(u'Grafica de barras')  # Figure
        ax = fig.add_subplot(111)  # Axes

        number_of_points = []
        count = []
        for k, v in self.route_collection.points.iteritems():
            number_of_points.append(k)
            count.append(v)
        xx = range(len(count))

        ax.bar(xx, count, width=0.8, align='center')
        ax.set_yticks(np.arange(min(count), max(count) + 1, 1.0))
        ax.set_xticks(xx)
        ax.set_xticklabels(number_of_points)

        plt.show()

    def paintUneveness(self):
        lista = []
        actual_point = self.route_collection.points[0]
        variable_x = 0

        for point in self.route_collection.points:
            variable_x = actual_point.meters_to_point(point) + variable_x
            variable_y = point.elevation
            actual_point = point
            lista.append([variable_x, variable_y])
        lista = np.array(lista)
        for point in lista:
            plt.plot(lista[:, 0], lista[:, 1], c="red")
        plt.xlabel('Distance in meters')
        plt.ylabel('Elevation')
        plt.show()
