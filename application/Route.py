import json

from application.Area import Area
from application.GpxFile import GpxFile
from application.Point import Point


class Route:

    def __init__(self, points: [Point], name='noname'):
        self.points = points
        self.name = name
        self.length = 0
        self.duration = 0
        self.unevenness = 0
        self.positiveUnevenness = 0
        self.negativeUnevenness = 0
        self.area = Area(self.points)
        self.__read_duration()
        self.length = self.__read_length()
        self.__read_unevenness()
        self.__read_direction_of_points()
        self.velocity = 0 if self.duration == 0 else self.length / (self.duration * 60)

    @classmethod
    def from_file_name(cls, folder, name):
        path = folder + '/%s.gpx' % name
        file = GpxFile.read_by_path(path)
        return cls(file.points, file.name + str(name))

    def __read_duration(self):
        self.initial_point = self.points[0]
        last_point = self.points[len(self.points) - 1]
        self.duration = self.initial_point.minutes_to_point(last_point)

    def __read_length(self):
        accumulate_distance_between_points = 0
        for distance_of_point in self.distances_between_points():
            accumulate_distance_between_points = distance_of_point + accumulate_distance_between_points
        return accumulate_distance_between_points

    def __read_unevenness(self):
        accumulate_unevenness = 0
        accumulate_positive_unevenness = 0
        accumulate_negative_unevenness = 0
        actual_point = self.initial_point
        for point in self.points:
            unevenness = actual_point.elevation_to_point(point)
            if unevenness > 0:
                accumulate_positive_unevenness = unevenness + accumulate_positive_unevenness
            elif unevenness < 0:
                accumulate_negative_unevenness = unevenness + accumulate_negative_unevenness
            accumulate_unevenness = abs(unevenness) + accumulate_unevenness
            actual_point = point
        self.unevenness = accumulate_unevenness
        self.positiveUnevenness = accumulate_positive_unevenness
        self.negativeUnevenness = accumulate_negative_unevenness

    def __read_direction_of_points(self):
        direction_of_points = []
        direction_variation_of_points = []
        previous_point = self.initial_point
        for point in self.points[1:]:
            point.direction_point(previous_point)
            direction_of_points.append(point.direction)
            direction_variation_of_points.append(point.direction_variation)
            previous_point = point
        self.direction_of_points = direction_of_points
        self.direction_variation_of_points = direction_variation_of_points

    def distances_between_points(self):
        distances_of_points = []
        actual_point = self.initial_point
        for point in self.points[1:]:
            distances_of_points.append(actual_point.meters_to_point(point))
            actual_point = point
        return distances_of_points

    def time_between_points(self):
        time_between_points = []
        actual_point = self.initial_point
        for point in self.points[1:]:
            time_between_points.append(actual_point.seconds_to_point(point))
            actual_point = point
        return time_between_points

    def velocity_between_points(self):
        velocity_between_points = []
        previous_point = self.initial_point
        for point in self.points[1:]:
            velocity_between_points.append(previous_point.velocity_to_point(point))
            previous_point = point
        return velocity_between_points

    def number_of_points(self):
        return len(self.points)

    def to_file(self, name: str):
        GpxFile.save_by_route(self, name)

    def __str__(self):
        return json.dumps(self.__dict__, default=str)
