import osmnx
from scipy.spatial import KDTree

from application.CsvFile import CsvFile
from application.Point import Point


class Shape:

    def __init__(self, area):
        self.__points = []
        self.__latitudesAndLongitudes = []
        self.__init_shape(area)

    def __init_shape(self, area):
        self.__read_shape(area)
        self.__generate_points()
        self.__build_tree()

    def __read_shape(self, area):
        graph = osmnx.graph_from_polygon(area.polygon)
        self.__nodes, self.__edges = osmnx.graph_to_gdfs(graph)

    def __generate_middle_points(self, p1, p2, parts):
        x_spacing = (p2.longitude - p1.longitude) / parts
        y_spacing = (p2.latitude - p1.latitude) / parts

        for i in range(1, parts):
            p = Point(p1.latitude + i * y_spacing, p1.longitude + i * x_spacing)
            self.__append_point_to_list(p)

    def __append_point_to_list(self, p: Point):
        self.__points.append(p)
        self.__latitudesAndLongitudes.append([p.longitude, p.latitude])

    def __generate_points(self):
        for i, edge in self.__edges.iterrows():
            last_point = None
            for x, y in edge.geometry.coords:
                current_point = Point(y, x)
                if last_point is not None:
                    number_of_middle_points = int(round(last_point.meters_to_point(current_point))) + 1
                    self.__generate_middle_points(last_point, current_point, number_of_middle_points)
                    self.__append_point_to_list(current_point)
                last_point = current_point

    def __build_tree(self):
        self.__tree = KDTree(self.__latitudesAndLongitudes)

    def save_in_file(self):
        CsvFile(self.__points, 'Real points')

    def extract_near_point_and_distance(self, point: Point):
        nearest_distance_point, nearest_value_position = self.__tree.query([point.longitude, point.latitude])
        nearest_point = Point(self.__tree.data.item(nearest_value_position, 1), self.__tree.data.item(nearest_value_position, 0))
        distance = nearest_point.meters_to_point(point)
        return nearest_point, distance
