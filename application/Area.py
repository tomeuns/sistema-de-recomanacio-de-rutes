import json

from shapely.geometry import Polygon

from application.Point import Point


class Area:

    def __init__(self, points):
        self.from_point = self.__calculate_minimum_point(points)
        self.to_point = self.__calculate_maximum_point(points)
        self.polygon = Polygon.from_bounds(self.from_point.longitude, self.from_point.latitude, self.to_point.longitude, self.to_point.latitude)

    @staticmethod
    def __calculate_minimum_point(points):
        latitude = 99999999999
        longitude = 99999999999
        for point in points:
            if point.latitude < latitude:
                latitude = point.latitude
            if point.longitude < longitude:
                longitude = point.longitude
        return Point(latitude, longitude)

    @staticmethod
    def __calculate_maximum_point(points):
        latitude = 0
        longitude = 0
        for point in points:
            if point.latitude > latitude:
                latitude = point.latitude
            if point.longitude > longitude:
                longitude = point.longitude
        return Point(latitude, longitude)

    def __str__(self):
        return json.dumps(self.__dict__, default=str)

