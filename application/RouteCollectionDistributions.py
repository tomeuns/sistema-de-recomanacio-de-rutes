from application.DistributionGraph import DistributionGraph
from application.DistributionGraphData import DistributionGraphData
from application.PointsDistribution import PointsDistribution
from application.RouteCollection import RouteCollection
from application.Shape import Shape


class RouteCollectionDistributions:

    def __init__(self, name, route_collection: RouteCollection):
        self.name = name
        self.shape = Shape(route_collection.area)
        self.__init_points_distribution = PointsDistribution(route_collection.initial_points, route_collection.area)

        self.__distance_between_points_distribution = route_collection.distance_between_points_distribution()
        self.__time_between_points_distribution = route_collection.time_between_points_distribution()
        self.__velocity_between_points_distribution = route_collection.velocity_between_points_distribution()

        self.__distance_of_route_distribution = route_collection.distance_distribution()
        self.__duration_of_route_distribution = route_collection.duration_distribution()
        self.__velocity_of_route_distribution = route_collection.velocity_of_route_distribution()

        self.__direction_of_points_distribution = route_collection.direction_of_points_distribution()
        self.__direction_variation_of_points_distribution = route_collection.direction_variation_of_points_distribution()

        self.__distance_to_real_point_distribution = route_collection.distance_to_real_point_distribution(self.shape)

        self.__list = [self.__distance_between_points_distribution, self.__time_between_points_distribution,
                       self.__velocity_between_points_distribution, self.__distance_of_route_distribution,
                       self.__duration_of_route_distribution, self.__velocity_of_route_distribution,
                       self.__direction_of_points_distribution, self.__direction_variation_of_points_distribution,
                       self.__distance_to_real_point_distribution]

    def generate_init_point_by_probability(self):
        return self.__init_points_distribution.generate_by_probability()

    def generate_distance_of_route_by_probability(self):
        return self.__distance_of_route_distribution.value_of_probability()

    def generate_duration_of_route_by_probability(self):
        return self.__duration_of_route_distribution.value_of_probability()

    def generate_velocity_of_route_by_probability(self):
        return self.__velocity_of_route_distribution.value_of_probability()

    def generate_distance_between_points_by_probability(self):
        return self.__distance_between_points_distribution.value_of_probability()

    def generate_time_between_points_by_probability(self):
        return self.__time_between_points_distribution.value_of_probability()

    def generate_velocity_between_points_by_probability(self):
        return self.__velocity_between_points_distribution.value_of_probability()

    def generate_direction_variation_of_points_by_probability(self):
        return self.__direction_variation_of_points_distribution.value_of_probability()

    def generate_distance_to_real_point_by_probability(self) -> float:
        return self.__distance_to_real_point_distribution.value_of_probability()

    def create_graphs_of_distributions(self):
        # self.__distance_between_points_distribution.plot_image(self.name)
        # self.__time_between_points_distribution.plot_image(self.name)
        # self.__velocity_between_points_distribution.plot_image(self.name)
        # self.__distance_of_route_distribution.plot_image(self.name)
        # self.__duration_of_route_distribution.plot_image(self.name)
        # self.__direction_of_points_distribution.plot_image(self.name)
        # self.__direction_variation_of_points_distribution.plot_image(self.name)
        # self.__distance_to_real_point_distribution.plot_image(self.name)
        # self.__init_points_distribution.plot_image(self.name)
        None

    @staticmethod
    def create_graphs_of_multiple_distributions(*routes_collections_distributions):
        for i in range(9):
            title = ''
            units = ''
            distribution_graph_data = []
            max_value, min_value = None, None
            for rcd in routes_collections_distributions:
                distribution = rcd.__list[i]
                title = distribution.title
                units = distribution.units
                data = distribution.get_data()
                max_value = distribution.max_value
                min_value = distribution.min_value
                distribution_graph_data.append(DistributionGraphData(data[0], data[1], rcd.name))
            DistributionGraph(title, units, *distribution_graph_data, distribution_type=None, show_legend=True, max_value=max_value,
                              min_value=min_value)
