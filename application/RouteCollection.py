import json
import os

from application.Area import Area
from application.Distribution import Distribution
from application.Route import Route
from application.Shape import Shape


class RouteCollection:
    __maximum_length_route = 30000  # meters
    __maximum_duration_route = 900  # minutes
    __maximum_route_velocity = 2  # meters/second
    __maximum_time_between_points = 75  # seconds
    __maximum_distance_between_points = 50  # meters
    __maximum_velocity_between_points = 4  # meters/second
    __maximum_direction_of_points = 360  # degrees
    __minimum_direction_of_points = -360  # degrees
    __maximum_distance_between_gps_and_real_point = 500  # meters

    def __init__(self, routes: [Route]):
        self.__routes = routes
        self.__read_points()
        self.area = Area(self.points)
        self.__read_init_points()

    @classmethod
    def read_routes(cls, zone='massanella', limit=None):
        path = '../data/' + zone
        routes_names = cls.get_routes_files_to_read(path, limit)
        routes = []
        for route_name in routes_names:
            route = Route.from_file_name(path, route_name)
            if RouteCollection.is_valid_route(route):
                routes.append(route)
        return cls(routes)

    # def :
    #
    # # return [88121, 125713, 296404]
    # # return [88121, 125713, 296404, 323234, 328900, 345973]
    # # return [88121, 125713, 296404, 323234, 328900, 345973, 354996]
    # # return [88121, 125713, 296404, 323234, 328900, 345973, 354996, 474645, 488775, 636701, 738081, 739617, 746106, 773328]
    # return all_routes_codes()

    def __read_points(self):
        self.points = []
        for route in self.__routes:
            for point in route.points:
                self.points.append(point)

    def distance_distribution(self):
        routes_lengths = []
        for route in self.__routes:
            routes_lengths.append(route.length)
        return Distribution(routes_lengths, 'a route length', 'Meters', RouteCollection.__maximum_length_route, 0)

    def duration_distribution(self):
        times = []
        for route in self.__routes:
            times.append(route.duration)
        return Distribution(times, 'a route duration time', 'Minutes', RouteCollection.__maximum_duration_route, 0)

    def distance_between_points_distribution(self):
        distances_points = []
        for route in self.__routes:
            distances_points.extend(route.distances_between_points())
        return Distribution(distances_points, 'the distance between points', 'Meters',
                            RouteCollection.__maximum_distance_between_points, 0)

    def direction_of_points_distribution(self):
        direction_of_points = []
        for route in self.__routes:
            direction_of_points.extend(route.direction_of_points)
        return Distribution(direction_of_points, 'the direction of points', 'Degrees',
                            RouteCollection.__maximum_direction_of_points, self.__minimum_direction_of_points)

    def direction_variation_of_points_distribution(self):
        difference_direction_points = []
        for route in self.__routes:
            difference_direction_points.extend(route.direction_variation_of_points)
        return Distribution(difference_direction_points, 'the direction variation of points', 'Degrees',
                            self.__maximum_direction_of_points, self.__minimum_direction_of_points)

    def time_between_points_distribution(self):
        times_between_points = []
        for route in self.__routes:
            times_between_points.extend(route.time_between_points())
        return Distribution(times_between_points, 'time between points', 'Seconds',
                            RouteCollection.__maximum_time_between_points, 0)

    def velocity_between_points_distribution(self):
        velocity_between_points = []
        for route in self.__routes:
            velocity_between_points.extend(route.velocity_between_points())
        return Distribution(velocity_between_points, 'velocity between points', 'meters / second',
                            RouteCollection.__maximum_velocity_between_points, 0)

    def distance_to_real_point_distribution(self, shape: Shape):
        distance_to_real_point = []
        for point in self.points:
            nearest_point, distance = shape.extract_near_point_and_distance(point)
            distance_to_real_point.append(distance)
        print('Se han analizado ' + str(len(self.points)) + ' puntos.')
        return Distribution(distance_to_real_point, 'distance between gps to real point', 'Meters',
                            self.__maximum_distance_between_gps_and_real_point, 0)

    def velocity_of_route_distribution(self):
        velocity_of_routes = []
        for route in self.__routes:
            velocity_of_routes.append(route.velocity)
        return Distribution(velocity_of_routes, 'a route velocity', 'meters / second', self.__maximum_route_velocity, 0)

    def __read_init_points(self):
        self.initial_points = []
        for route in self.__routes:
            self.initial_points.append(route.initial_point)

    def __str__(self):
        return json.dumps(self.__dict__, default=str)

    def print_routes_information_to_debug(self):
        for route in self.__routes:
            print(str(route.length) + ' *** ' + str(route.velocity) + ' ***** ' + route.name)

    @staticmethod
    def is_valid_route(route: Route) -> bool:
        return route.velocity <= RouteCollection.__maximum_route_velocity and route.length <= RouteCollection.__maximum_length_route

    def save(self, routes_to_save: int, suffix_name=''):
        maximum = len(self.__routes)
        limit = routes_to_save if routes_to_save and routes_to_save < maximum else maximum
        for index in range(limit):
            self.__routes[index].to_file("my-route-" + str(index) + suffix_name)

    @staticmethod
    def get_routes_files_to_read(path, limit: int):
        files = []
        for file in os.listdir(path):
            if (limit is None or len(files) < limit) and file.endswith(".gpx"):
                files.append(os.path.splitext(file)[0])
        return files
