import gpxpy
from gpxpy.gpx import GPXTrackPoint, GPXTrackSegment, GPX, GPXTrack

from application.Point import Point


class GpxFile:

    def __init__(self, points, name='no_name'):
        self.name = self.sanitize(name)
        self.points = points

    @classmethod
    def save_by_route(cls, route, file_name: str):
        gpx_file = cls(route.points, file_name)
        gpx_file.__generate_file()

    @classmethod
    def read_by_path(cls, path):
        with open(path, 'r') as file:
            gps_route = gpxpy.parse(file)
            gps_route.reduce_points(min_distance=0.5)
            gps_route.add_missing_speeds()
            # gps_route.simplify()  # Eliminar linea
        return cls.__read_points_of_route(gps_route)

    def __generate_file(self):
        file = open(self.name + '.gpx', "w")
        file.write(self.__generate_xml())
        file.close()

    def __generate_xml(self):
        gpx = GPX()
        gpx_track = GPXTrack()
        gpx.tracks.append(gpx_track)
        gpx_segment = GPXTrackSegment()
        gpx_track.segments.append(gpx_segment)
        for point in self.points:
            gpx_segment.points.append(GPXTrackPoint(point.latitude, point.longitude, point.elevation))
        return gpx.to_xml()

    @classmethod
    def __read_points_of_route(cls, gps_route):
        points = []
        track = gps_route.tracks[0]
        name = track.name
        for segment in track.segments:
            for gpx_point in segment.points:
                p = Point.from_gpx(gpx_point)
                points.append(p)
        return cls(points, name)

    def sanitize(self, file_name):
        return ''.join(c.lower() for c in file_name if not c.isspace())
