from matplotlib import pyplot as plt


class DistributionGraph:

    def __init__(self, title, units, *data, distribution_type=None, show_legend=False, max_value=None, min_value=None):
        self.title = title
        self.units = units
        self.distribution_type = distribution_type
        self.data = data
        self.showLegend = show_legend
        self.max_value = max_value
        self.min_value = min_value
        self.__plot_image()

    def __plot_image(self):
        plot_title = self.__generate_title()
        plt.figure(self.title)
        plt.title(plot_title)
        self.__plot_data()
        plt.ylabel('Probability')
        plt.xlabel(self.units)
        self.set_limits()
        if self.showLegend:
            plt.legend()
        plt.savefig(plot_title.replace(" ", "_"))

    def set_limits(self):
        minimum, maximum = self.__get_data_limits()
        if self.max_value > maximum and maximum is not None:
            self.max_value = maximum
        if self.min_value < minimum and minimum is not None:
            self.min_value = minimum
        margin = 0.1 * (self.max_value - self.min_value)
        plt.xlim(self.min_value - margin, self.max_value + margin)

    def __plot_data(self):
        for data_value in self.data:
            plt.plot(data_value.x, data_value.y, label=data_value.legend)

    def __generate_title(self):
        return "Distribution of " + self.title + self.__add_distribution_information()

    def __add_distribution_information(self):
        return " by " + self.distribution_type + " routes" if self.distribution_type else ''

    def __get_data_limits(self):
        minimum = None
        maximum = None
        for line in self.data:
            minimum_line = line.x[1]
            maximum_line = line.x[len(line.x) - 1]
            if minimum is None or minimum_line < minimum:
                minimum = minimum_line
            if maximum is None or maximum_line > maximum:
                maximum = maximum_line
        return minimum, maximum
