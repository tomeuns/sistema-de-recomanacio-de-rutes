import geopy
from geopy import Point
from geopy.distance import GeodesicDistance

from application.Point import Point
from application.Route import Route
from application.RouteCollection import RouteCollection
from application.RouteCollectionDistributions import RouteCollectionDistributions
from application.Shape import Shape


class GenerateRoutesService:

    def __init__(self, rcd: RouteCollectionDistributions, number_of_routes=1, active_reals_points=False):
        self.active_real_points = active_reals_points
        self.__rcd = rcd
        self.__shape = rcd.shape
        self.__number_of_routes = number_of_routes

    def generate(self) -> RouteCollection:
        routes = []
        for _ in range(self.__number_of_routes):
            routes.append(self.__generate_route())
        return RouteCollection(routes)

    def __generate_route(self) -> Route:
        points = []
        total_distance = 0
        route_distance = self.__rcd.generate_distance_of_route_by_probability()
        previous_point = self.__rcd.generate_init_point_by_probability()
        #TODO añadir un angulo inicial por defecto para que no empiezen todas igual
        while total_distance < route_distance:
            points.append(previous_point)
            point = self.__generate_point_with_probability(previous_point)
            total_distance = total_distance + previous_point.meters_to_point(point)
            previous_point = point
        return Route(points)

    def __generate_point_with_probability(self, previous_point: Point) -> Point:
        generated_point = self.generate_random_point(previous_point)
        if self.active_real_points:
            for x in range(0, 1000):
                generated_distance = self.__generated_point_distance_to_near_real_point(generated_point)
                distance_by_probability = self.__rcd.generate_distance_to_real_point_by_probability()
                if generated_distance <= distance_by_probability:
                    return generated_point
                generated_point = self.generate_random_point(previous_point)
        return generated_point

    def generate_random_point(self, previous_point) -> Point:
        distance = self.__rcd.generate_distance_between_points_by_probability()
        direction_variation = self.__rcd.generate_direction_variation_of_points_by_probability()
        generated_point = self.__generate_point_by_distance_and_angle(previous_point, distance, direction_variation)
        velocity = self.__rcd.generate_velocity_between_points_by_probability()
        generated_point.update_time_and_direction(velocity, previous_point)
        return generated_point

    def __generated_point_distance_to_near_real_point(self, generated_point) -> float:
        p, d = self.__shape.extract_near_point_and_distance(generated_point)
        return d

    @staticmethod
    def __generate_point_by_distance_and_angle(previous_point: Point, distance, difference_angle) -> Point:
        angle = previous_point.direction + difference_angle
        geopy_point = geopy.Point(previous_point.latitude, previous_point.longitude)
        generated_geopy_point = GeodesicDistance(meters=distance).destination(geopy_point, angle)
        return Point(generated_geopy_point.latitude, generated_geopy_point.longitude)
