import datetime
import json

from geographiclib.geodesic import Geodesic
from geopy.distance import distance


class Point:

    def __init__(self, latitude: float, longitude: float, time=datetime.datetime.min, elevation=0):
        self.latitude = latitude
        self.longitude = longitude
        self.time = time
        self.elevation = elevation
        self.direction = 0
        self.direction_variation = 0

    @classmethod
    def from_gpx(cls, gpx_point):
        return cls(gpx_point.latitude, gpx_point.longitude, gpx_point.time, gpx_point.elevation)

    def meters_to_point(self, point):
        from_point = (self.latitude, self.longitude)
        to_point = (point.latitude, point.longitude)
        return distance(from_point, to_point).meters

    def seconds_to_point(self, point) -> float:
        time = abs(point.time - self.time)
        milliseconds = time.seconds * 1000 + time.microseconds / 1000.0
        return milliseconds / 1000.0

    def minutes_to_point(self, point):
        return self.seconds_to_point(point) / 60

    def elevation_to_point(self, point):
        return point.elevation - self.elevation

    def velocity_to_point(self, point):
        distance_to_point = self.meters_to_point(point)
        time = self.seconds_to_point(point)
        return 0 if time == 0 else distance_to_point / time

    def direction_point(self, previous_point):
        self.direction = Geodesic.WGS84.Inverse(previous_point.latitude, previous_point.longitude, self.latitude, self.longitude)['azi1']
        self.direction_variation = previous_point.direction - self.direction

    def update_time_and_direction(self, velocity, previous_point):
        distance_of_points = self.meters_to_point(previous_point)
        difference_seconds = 0 if velocity == 0 else distance_of_points / velocity
        self.time = previous_point.time + datetime.timedelta(0, difference_seconds)
        self.direction_point(previous_point)

    def match(self, point):
        return self.meters_to_point(point) < 0.5

    def __eq__(self, other):
        return self.latitude == other.latitude and self.longitude == other.longitude

    def __hash__(self):
        return hash((self.latitude, self.longitude))

    def __str__(self):
        return json.dumps(self.__dict__, default=str)
